#include <stdint.h>
#include <stdlib.h> /* EXIT_FAILURE, EXIT_SUCCESS, strtoull() */
#include <limits.h> /* strtoull() */
#include <string.h> /* memcpy() */
#include <stdio.h> /* printf() */

static uint64_t tbl[64][8][256];

/* Welcome to hell! */
int main(int argc, char *argv[]) {
	uint64_t i, n, t, w, tn;

	/* Get the number. */
	if (argc < 4) {
		fprintf(stderr, "give prefix and number\n");
		return EXIT_FAILURE;
	}
	w = strtoull(argv[2], NULL, 10);
	tn = strtoull(argv[3], NULL, 10);

	/* Make checksum table. */
	for (i = 0; i < 256; ++i) {
		tbl[0][0][i] = i;
		for (n = 0; n < 8; ++n)
			tbl[0][0][i] = (tbl[0][0][i] >> 1) ^
			               (-(tbl[0][0][i] & 1) & 0xC96C5795D7870F42);
	}

	/* Make tables for 1-7 zeros. */
	for (n = 1; n < 8; ++n) {
		memcpy(tbl[0][n], tbl[0][n - 1], sizeof(tbl[0][n]));
		for (i = 0; i < 256; ++i)
			tbl[0][n][i] = tbl[0][0][tbl[0][n][i] & 0xFF] ^ (tbl[0][n][i] >> 8);
	}

	/* Make tables for 2^3 until 2^64 zeros. */
	for (n = 1; n < 64; ++n) for (t = 0; t < 8; ++t) for (i = 0; i < 256; ++i) {
		const uint64_t ca = tbl[n - 1][t][i];
		tbl[n][t][i] = tbl[n - 1][0][(ca >> 56) & 0xFF] ^
		               tbl[n - 1][1][(ca >> 48) & 0xFF] ^
		               tbl[n - 1][2][(ca >> 40) & 0xFF] ^
		               tbl[n - 1][3][(ca >> 32) & 0xFF] ^
		               tbl[n - 1][4][(ca >> 24) & 0xFF] ^
		               tbl[n - 1][5][(ca >> 16) & 0xFF] ^
		               tbl[n - 1][6][(ca >>  8) & 0xFF] ^
		               tbl[n - 1][7][(ca >>  0) & 0xFF];
	}

	/* Compose the requested table. */
	uint64_t tblres[256];
	memcpy(tblres, tbl[0][0], sizeof(tblres));
	for (n = 0; n < 64 - 4; ++n) {
		if (tn & ((uint64_t) 0x10 << n)) {
			for (i = 0; i < 256; ++i) {
				const uint64_t ca = tblres[i];
				tblres[i] = tbl[n + 1][0][(ca >> 56) & 0xFF] ^
				            tbl[n + 1][1][(ca >> 48) & 0xFF] ^
				            tbl[n + 1][2][(ca >> 40) & 0xFF] ^
				            tbl[n + 1][3][(ca >> 32) & 0xFF] ^
				            tbl[n + 1][4][(ca >> 24) & 0xFF] ^
				            tbl[n + 1][5][(ca >> 16) & 0xFF] ^
				            tbl[n + 1][6][(ca >>  8) & 0xFF] ^
				            tbl[n + 1][7][(ca >>  0) & 0xFF];
			}
		}
	}
	for (n = 0, t = tn & 0x0F; n < t; ++n) for (i = 0; i < 256; ++i)
		tblres[i] = tbl[0][0][tblres[i] & 0xFF] ^ (tblres[i] >> 8);

	/* Print it out. */
	for (i = 0; i < 256; ++i) {
		if ((i + 1) % w == 1 && i == 255)
			printf("%s0x%016llX,\n", argv[1], (unsigned long long) tblres[i]);
		else if ((i + 1) % w == 0 || i == 255)
			printf("0x%016llX,\n", (unsigned long long) tblres[i]);
		else if ((i + 1) % w == 1)
			printf("%s0x%016llX, ", argv[1], (unsigned long long) tblres[i]);
		else printf("0x%016llX, ", (unsigned long long) tblres[i]);
	}

	return EXIT_SUCCESS;
}
