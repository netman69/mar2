#ifndef __MZ_H__
#define __MZ_H__

#include <stdint.h>

/* Macro to determine maximum output size of compression function.
 * 4 bytes are added because that's how much a single sequence with only
 * incompressible data will grow at most. Beyond that the other condition that
 * could cause an output bigger than the input is when an incompressible part
 * of 270 bytes or more is followed by just 4 bytes that get compressed and
 * another incompressible section of 270 or more bytes that would otherwise be
 * part of the sequence before it, the output can grow by 2 bytes. Hence we add
 * size / 540 * 2, it's okay the result of the division is rounded down because
 * we do really need 540 bytes to add 2. There are intermediate situations
 * where the input doesn't grow but having matches does not shrink the file.
 * Ideally the compressor prevents all growth except the maximum of 4 bytes for
 * an incompressible sequence mentioned, but this macro should remain as is
 * since it should account for what the format supports rather than what the
 * compression does.
 */
#define MZ_OUTPUTMAX(isize) ((isize) + 4 + (isize) / 540 * 2)

/* Also a macro to determine the input size that'd generate a maximum given
 * output size. Again we don't care the result of division is rounded down,
 * means the size could be a byte smaller than needed to entirely fill the
 * buffer size given when compressed, only bigger would be a problem.
 */
#define MZ_INPUTMAX(osize) ((270 * osize - 1080) / 271)

/* Hashmap needed for the compress function, initialization matters not. */
typedef uint16_t mz_map_t[0x10000];

/* With both functions prev and prev_len pertain a buffer holding the previous
 * chunk of uncompressed data, if it was given with mz_compress it should also
 * be given with decompress. Compression ratio suffers if no prev is given as
 * would be communicated to the function by setting prev_len to 0, but it can
 * be useful to compress chunks of data without having the output chunks be
 * dependent on decompressing all previous chunks.
 * 
 * Return value of the functions is the output length, compressed data has to
 * be fed to the decompression function as the same size and divided in the
 * same chunks as it came out of the compression function, so if the input is
 * broken up into chunks for compression, some sort of framing is neccessary.
 * 
 * Neither function throws errors, memory allocation is entirely static so not
 * much can go wrong, but framing should include a checksum of sorts if data
 * integrity is to be assured. Care should be taken to give the decompress
 * function an output buffer that is as large as the largest possible input
 * used to compress, if an operation needs more space than out_len it will stop
 * at once, even when there is less than out_len bytes already decompressed.
 * 
 * Inputs to the compress function are at most 65536 (0x10000) bytes, because
 * the hashmap it uses for compressing has entries of that size. Changing that
 * would impact performance. Verifying the length of inputs is up to the user.
 * 
 * In addition to using the prev argument for previous blocks, it can be used
 * for a dictionary, to aid in compressing small files. For compression the
 * dictionary should first be indexed into the hashmap, with mz_loaddict().
 * The compress function will overwrite the resulting hashmap, so if it is to
 * be re-used a copy of the map should be made for each use (memcpy is likely
 * faster than loaddict). Like inputs the dictionary can be at most 65536 bytes
 * long, verifying this length is up to the user.
 */

extern void mz_loaddict(mz_map_t *map, uint8_t *in, uint32_t in_len);

extern uint32_t mz_compress(mz_map_t *map, uint8_t *out,
                            uint8_t *in, uint32_t len,
                            uint8_t *prev, uint32_t prev_len);

extern uint32_t mz_decompress(uint8_t *out, uint32_t out_len,
                              uint8_t *prev, uint32_t prev_len,
                              uint8_t *in, uint32_t in_len);

#endif /* __MZ_H__ */
