CFLAGS  += -g -O2 -std=c99 -Wall -pedantic
OBJS_MZ  = mz.o crc64.o tables.o main.o
OBJS_MAR = mz.o crc64.o tables.o mar2.o
OBJS     = $(OBJS_MZ) $(OBJS_MAR)

all: mz mar

mz: $(OBJS_MZ)
	$(CC) -o $@ $(LDFLAGS) $(OBJS_MZ)

mar: $(OBJS_MAR)
	$(CC) -o $@ $(LDFLAGS) $(OBJS_MAR)

tables.c: tablegen
	./mktables.sh > $@

clean:
	rm -rf $(OBJS) mz mar tablegen tables.c
