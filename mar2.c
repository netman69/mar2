/* mar - The Mat ARchiver.
 * 
 * TODO
 * - progress display a la mz utility
 * - documentation documentation documentation
 * - install target for makefile
 * - document file format, how traversing deals with . and .. (treats the folder as root)
 * - have option to manually turn on/off using index
 * - have test function also test index and whether it matches contents
 * - do we want mtime/uid/gid stored?
 * - atm you could carefully take out 1 file and still have all CRC match, should we fix that?
 * - should level be uint16_t? what about flags?
 * - go over all the globals and decide whether we really need em
 * - better error handling
 * - perhaps symlinks should be printable too
 * - proper main function
 * - option like -C in tar
 * - disable writing compressed garbage to tty unless told to
 * - make sure addfile doesn't fail silently on some errors
 * - options to get more info from archive
 * - should we clean it up and get rid of all the globals so that we can have a library?
 * - should the mzip cmd line stuff become also like a library thing?
 */

#define _DEFAULT_SOURCE
#include <sys/types.h> /* opendir(), closedir(), mkfifo() */
#include <sys/stat.h> /* stat(), mkdir(), mkfifo() */
#include <dirent.h> /* opendir(), closedir(), readdir() */
#include <fcntl.h> /* open() */
#include <errno.h> /* errno */
#include <stddef.h> /* NULL, size_t */
#include <string.h> /* strcmp(), strlen(), strncpy(), memset(), memcpy()
                       strchr() */
#include <stdint.h> /* uint32_t, uint64_t */
#include <unistd.h> /* STDIN_FILENO, STDOUT_FILENO, read(), write(), lseek(),
                       close(), readlink(), symlink(), unlink() */
#include <stdio.h> /* printf(), fprintf(), perror() */
#include <limits.h> /* PATH_MAX */
#include <stdlib.h> /* realloc() */
#include "mz.h"
#include "crc64.h"
#include "end.h"
#include "time64.h"

#define ADDFILES_DEPTH_MAX (PATH_MAX / 2)
#define MINSIZE_COMPRESS (5) // TODO determine what should be the minimum

/* we use different CRC initializers for different types of data so that they
 * can't be mixed up even if something went horribly wrong, in some unlikely
 * situations this could prevent creation of garbage files or confusing bugs
 */
#define CRCINIT_HEAD 0x40BFB84683D11676U
#define CRCINIT_FHDR 0x17D3103A117C1654U
#define CRCINIT_DATA 0x63F7E0BE35ED8649U /* compressed file data */
#define CRCINIT_FILE 0x791BD4CB92D81736U /* uncompressed file */
#define CRCINIT_IDX  0x8C1BB327BB1D1A83U
#define CRCINIT_TAIL 0xB56A9486033CEFA3U

typedef struct __attribute__((__packed__)) {
	uint32_t magic, flags;
	uint64_t crc; /* always checked to ensure no false no-crc flag */
} header_t;

#define MAGIC (0x4d41525aU)
#define FLAG_NOCRC ((uint64_t) 1 << 31)
#define FLAG_NOIDX ((uint64_t) 1 << 30)

typedef struct __attribute__((__packed__)) {
	char name[256];
	uint64_t offset; /* relative to start of file */
	uint32_t level; /* 0 is the root of the package */
} file_idx_t;

typedef struct __attribute__((__packed__)) {
	char name[256];
	uint32_t flags;
	uint32_t level; /* 0 is the root of the package */
	uint64_t size; /* uncompressed size */
	uint32_t nblk; /* number of compressed blocks, when applicable */
	uint32_t mode;
} file_hdr_t;

#define FILE_FLAG_UNCOMPRESSED (1 << 0)

typedef struct __attribute__((__packed__)) {
	uint64_t size; /* compressed size of the index so it can be found */
	uint64_t crc; /* crc of size, written regardless of opt_crc */
} idx_tail_t;

typedef struct __attribute__((__packed__)) {
	uint16_t len; /* stores length - 1, end is indicated by file size */
	uint8_t data[0x10000 + 8]; /* + 8 for CRC */
} zdata_t;

/* do not change the size of buf_uc, we depend on it in many ways */
static uint8_t buf_uc[2][MZ_INPUTMAX(0x10000)];
static size_t buf_prevlen = 0;
static int bufno = 0;

#define buf_curr (buf_uc[bufno])
#define buf_prev (buf_uc[(bufno + 1) % 2])

static zdata_t zd;
static mz_map_t map = { 0 };
static file_idx_t *idx = NULL; // TODO free before exit for the hell of it
static uint64_t idx_len = 0;
static int fd = -1;
static size_t offset = 0;
static file_hdr_t file;
static char path[PATH_MAX + 1] = "";
static uint32_t level = 0;
static char *linktarget;

static int opt_verbose = 0;
static int opt_compress = 1;
static int opt_idx = 1;
static int opt_crc = 1;
static int opt_progress = 0;
static int opt_tty = 0;

static int have_crc = 0;
static int have_idx = 0;
static int have_seek = 0;

/* like strlcpy() */
static size_t mstrcpy(char *dst, char *src, size_t len) {
	size_t i;
	for (i = 0; src[i] != 0 && i + 1 < len; ++i)
		dst[i] = src[i];
	src[i] = 0;
	return i;
}

/* combine directory name and file name int a buf of size of PATH_MAX + 1 */
static ssize_t path_combine(char *path, char *file) {
	ssize_t i;
	for (i = 0; i < PATH_MAX - 1 && path[i] != 0; ++i);
	if (i != 0)
		path[i++] = '/';
	for (; i < PATH_MAX && *file != 0; ++i)
		path[i] = *file++;
	path[i] = 0;
	return (*file == 0) ? i : -1; /* returns -1 if things didn't fit */
}

/* remove last part of pathname */
static void path_dirup(char *path) {
	size_t i;
	for (i = strlen(path); i > 0 && path[i - 1] != '/'; --i);
	if (i > 0)
		path[i - 1] = 0;
	else path[0] = 0;
}

/* get last part of pathname */
static char *path_fname(char *path) {
	size_t i;
	for (i = strlen(path); i > 0 && path[i - 1] != '/'; --i);
	return path + i;
}

/* read that fails if the full count can't be read */
ssize_t mread(int fd, void *buf, size_t count) {
	while (count) {
		ssize_t r = read(fd, buf, count);
		if (r == 0) {
			errno = EIO;
			return -1;
		}
		if (r < 0)
			return -1;
		buf = ((char *) buf) + r;
		count -= r;
	}
	return 0;
}

/* new compression stream */
static void z_reset() {
	buf_prevlen = 0;
}

/* compress what's in buf_curr, write it, update offset and flip buffer */
static ssize_t z_write(uint64_t crcinit, size_t dlen) {
	size_t len = mz_compress(&map, zd.data, buf_curr, dlen,
	                         buf_prev, buf_prevlen);
	/* we don't compress 0 length files, so we can depend on len >= 1 */
	htole(&zd.len, len - 1, sizeof(zd.len));
	if (opt_crc) {
		uint64_t crc = crc64(crcinit, zd.data, len);
		htole(zd.data + len, crc, sizeof(crc));
		len += 8;
	}
	ssize_t ret = write(fd, &zd, len + sizeof(zd.len));
	if (ret < 0) {
		perror("write");
		return ret;
	}
	bufno = (bufno + 1) % 2;
	buf_prevlen = dlen;
	offset += ret;
	return ret;
}

/* read a compressed block, decompress, then flip buffer, result in buf_prev */
static ssize_t z_read(uint64_t crcinit) {
	size_t len;
	if (mread(fd, &zd.len, sizeof(zd.len)) < 0) {
		perror("read");
		return -1;
	}
	len = zd.len + 1;
	size_t rlen = len + (have_crc ? sizeof(uint64_t) : 0);
	if (mread(fd, zd.data, rlen) < 0) {
		perror("read");
		return -1;
	}
	offset += rlen + sizeof(zd.len); /* this one also updates offset */
	if (opt_crc && have_crc) {
		uint64_t crc = crc64(crcinit, zd.data, len);
		if (letoh(zd.data + len, sizeof(crc)) != crc) {
			fprintf(stderr, "checksum mismatch\n");
			return -1;
		}
	}
	len = mz_decompress(buf_curr, sizeof(buf_curr), buf_prev, buf_prevlen,
	                    zd.data, len);
	bufno = (bufno + 1) % 2;
	buf_prevlen = len;
	return len;
}

static int addfile(char *path, char *rpath, char *name, struct stat *sb,
                   uint32_t level) {
	uint32_t type = sb->st_mode & S_IFMT;
	uint64_t size = (type == S_IFREG) ? sb->st_size : 0, nblk = 0;
	uint32_t flags = (opt_compress && size > MINSIZE_COMPRESS) ?
	                 0 : FILE_FLAG_UNCOMPRESSED;
	file_hdr_t *hdr = (void *) buf_curr;
	size_t wlen = sizeof(*hdr);

	if (opt_verbose)
		fprintf(stderr, "%s\n", rpath);

	/* add to index */
	if (opt_idx) {
		if ((idx = realloc(idx, sizeof(file_idx_t) * (idx_len + 1))) == NULL) {
			idx_len = 0;
			perror("realloc");
			return -1;
		}
		file_idx_t *f = idx + idx_len++;
		memset(f->name, 0, sizeof(f->name));
		if (mstrcpy(f->name, name, sizeof(f->name)) == sizeof(f->name) - 1) {
			fprintf(stderr, "filename too long: %s\n", rpath);
			return -1;
		}
		htole(&f->offset, offset, sizeof(f->offset));
		htole(&f->level, level, sizeof(f->level));
	}

	/* if it's a link we read it already */
	if (type == S_IFLNK) {
		ssize_t lmax = sizeof(buf_curr) - sizeof(*hdr) - 1;
		void *dest = buf_curr + sizeof(*hdr);
		ssize_t ll = readlink(path, dest, lmax);
		if (ll >= lmax) {
			fprintf(stderr, "lint target too long: %s\n", rpath);
			return -1;
		}
		if (ll < 0) {
			perror("readlink");
			fprintf(stderr, "\twhile adding '%s'\n", rpath);
			return -1;
		}
		size = ll;
		wlen += ll;
	} else if (type != S_IFREG && type != S_IFDIR && type != S_IFIFO) {
		fprintf(stderr, "unsupported special file: %s\n", rpath);
		return -1;
	}

	/* write file header */
	memset(hdr->name, 0, sizeof(hdr->name));
	if (mstrcpy(hdr->name, name, sizeof(hdr->name)) == sizeof(hdr->name) - 1) {
		fprintf(stderr, "filename too long: %s\n", rpath);
		return -1;
	}
	if (nblk >> (sizeof(hdr->nblk) * 8)) { /* over 280 terrabytes, wow! */
		fprintf(stderr, "file too big: %s\n", rpath);
		return -1;
	}
	if (!(flags & FILE_FLAG_UNCOMPRESSED))
		nblk = size / sizeof(buf_curr) + (size % sizeof(buf_curr) ? 1 : 0);
	htole(&hdr->mode, sb->st_mode, sizeof(hdr->mode));
	htole(&hdr->size, size, sizeof(hdr->size));
	htole(&hdr->nblk, nblk, sizeof(hdr->nblk));
	htole(&hdr->level, level, sizeof(hdr->level));
	htole(&hdr->flags, flags, sizeof(hdr->flags));
	z_reset();
	if (z_write(CRCINIT_FHDR, wlen) < 0) /* this updates offset */
		return -1;

	/* if this wasn't a normal file we're done */
	if (type != S_IFREG)
		return 0;

	/* open the file */
	int in = open(path, O_RDONLY);
	if (in < 0) {
		perror("open");
		fprintf(stderr, "\twhile adding '%s'\n", rpath);
		return -1;
	}

	/* either compress or dump the file into output stream */
	if (flags & FILE_FLAG_UNCOMPRESSED) {
		uint64_t crc = CRCINIT_FILE;
		while (size > 0) {
			size_t bs = sizeof(buf_uc) > size ? size : sizeof(buf_uc);
			if (mread(in, buf_uc, bs) < 0) {
				perror("read");
				fprintf(stderr, "\twhile adding '%s'\n", rpath);
				goto failread;
			}
			if (opt_crc)
				crc = crc64(crc, buf_uc, bs);
			if (write(fd, buf_uc, bs) < 0) {
				perror("write");
				fprintf(stderr, "\twhile adding '%s'\n", rpath);
				goto failread;
			}
			size -= bs;
			offset += bs;
		}
		if (opt_crc) {
			htole(&crc, crc, 8);
			if (write(fd, &crc, 8) < 0) {
				perror("write");
				fprintf(stderr, "\twhile adding '%s'\n", rpath);
				goto failread;
			}
			offset += 8;
		}
	} else {
		z_reset();
		while (size > 0) {
			size_t bs = sizeof(buf_curr) > size ? size : sizeof(buf_curr);
			if (mread(in, buf_curr, bs) < 0) {
				perror("read");
				fprintf(stderr, "\twhile adding '%s'\n", rpath);
				goto failread;
			}
			if (z_write(CRCINIT_DATA, bs) < 0) /* handles CRC and offset */
				goto failread;
			size -= bs;
		}
	}
	close(in);
	return 0;
failread:
	close(in);
	return -1;
}

/* traverse filesystem and call addfile for each entry */
// TODO openat an fd first with O_NOFOLLOW, then fstat
// then fdopendir if it's a dir
// for symlinks open with O_NOFOLLOW will fail with ELOOP
//   note that it could also fail if there's just a too long series of
//   symlinks going to the path (likely not to encounter that except for the
//   initial path given to the function, since we don't follow symlinks)
// use readlinkat to avoid race conditions when reading symlinks
static int addfiles(char *path) {
	DIR *d[ADDFILES_DEPTH_MAX];
	int p = -1;
	uint32_t level = 0;
	struct dirent *e;
	struct stat sb;
	char fullpath[PATH_MAX + 1] = "";

	/* prepare path name stuff */
	if (path_combine(fullpath, path) < 0)
		goto errend;
	for (size_t i = strlen(fullpath); i > 0 && fullpath[i - 1] == '/'; --i)
		fullpath[i - 1] = 0;
	char *rpath = path_fname(fullpath);

	/* deal with root entry */
	if (lstat(fullpath, &sb) < 0)
		goto errend;
	if (strcmp(rpath, "..") == 0)
		rpath += 3;
	else if (strcmp(rpath, ".") == 0)
		rpath += 2;
	else if (addfile(fullpath, rpath, rpath, &sb, level++) < 0)
		goto errend;

	/* attempt to open path as a directory */
	if ((sb.st_mode & S_IFMT) == S_IFDIR && (d[++p] = opendir(path)) == NULL) {
		if (errno == ENOTDIR)
			return 0;
		else goto errend;
	}

	/* traverse directory tree */
	while (p >= 0) {
		while ((e = readdir(d[p])) != NULL) {
			if (strcmp(".", e->d_name) == 0 || strcmp("..", e->d_name) == 0)
				continue;
			if (path_combine(fullpath, e->d_name) < 0)
				goto errend;
			if (lstat(fullpath, &sb) < 0)
				goto errend;
			if (addfile(fullpath, rpath, e->d_name, &sb, level) < 0)
				goto errend;
			if ((sb.st_mode & S_IFMT) == S_IFDIR) {
				if (p >= ADDFILES_DEPTH_MAX) {
					errno = EIO;
					goto errend;
				}
				if ((d[++p] = opendir(fullpath)) == NULL) {
					--p;
					goto errend;
				}
				++level;
			} else path_dirup(fullpath);
		}
		closedir(d[p--]);
		path_dirup(fullpath);
		--level;
	}

	/* the end */
	return 0;
errend: // TODO make sure the user is told what happened on error
	while (p >= 0)
		closedir(d[p--]);
	return -1;
}

static int create(int npath, char **path) {
	/* write header */
	header_t hdr;
	uint32_t flags = 0;
	if (!opt_crc)
		flags |= FLAG_NOCRC;
	if (!opt_idx)
		flags |= FLAG_NOIDX;
	htobe(&hdr.magic, MAGIC, sizeof(hdr.magic));
	htole(&hdr.flags, flags, sizeof(hdr.flags));
	htole(&hdr.crc, crc64(CRCINIT_HEAD, &hdr, 8), sizeof(hdr.crc));
	if (write(fd, (void *) &hdr, sizeof(hdr)) < 0) {
		perror("write");
		return -1;
	}

	/* traverse and write files */
	offset = 0;
	for (int i = 0; i < npath; ++i)
		if (addfiles(path[i]) < 0)
			return -1;

	/* write end entry, lack of a filename indicates end */
	memset(buf_curr, 0, sizeof(file_hdr_t));
	z_reset();
	if (z_write(CRCINIT_FHDR, sizeof(file_hdr_t)) < 0)
		return -1;

	/* write index */
	if (opt_idx) {
		size_t len = idx_len * sizeof(file_idx_t), pos = offset, x = 0;
		idx_tail_t tail;
		z_reset();
		while (len > 0) {
			size_t bs = sizeof(buf_curr) > len ? len : sizeof(buf_curr);
			memcpy(buf_curr, (char *) idx + x, bs);
			if (z_write(CRCINIT_IDX, bs) < 0)
				return -1;
			len -= bs;
			x += bs;
		}
		htole(&tail.size, offset - pos, sizeof(tail.size));
		htole(&tail.crc, crc64(CRCINIT_TAIL, &tail.size, sizeof(tail.size)),
			  sizeof(tail.crc));
		if (write(fd, (void *) &tail, sizeof(tail)) < 0) {
			perror("write");
			return -1;
		}
	}

	return 0;
}

static int checkname(char *name, size_t size) {
	name[size - 1] = 0;
	if (strchr(name, '/') || strcmp(name, "..") == 0) {
		fprintf(stderr, "something is iffy about this archive, stopped\n");
		return -1;
	}
	return 0;
}

static int readfhdr() {
	file_hdr_t *hdr = (void *) buf_curr;
	ssize_t len;
	z_reset();
	if ((len = z_read(CRCINIT_FHDR)) < 0)
		return -1;
	if (len < (signed) sizeof(*hdr)) {
		fprintf(stderr, "file header too short\n");
		return -1;
	}
	memcpy(file.name, hdr->name, sizeof(file.name));
	if (checkname(file.name, sizeof(file.name)) < 0)
		return -1;
	file.flags = letoh(&hdr->flags, sizeof(file.flags));
	file.level = letoh(&hdr->level, sizeof(file.level));
	file.size = letoh(&hdr->size, sizeof(file.size));
	file.nblk = letoh(&hdr->nblk, sizeof(file.nblk));
	file.mode = letoh(&hdr->mode, sizeof(file.mode));
	if ((file.mode & S_IFMT) == S_IFLNK) {
		linktarget = (char *) (hdr + 1); /* valid until next next z_read() */
		if (file.size > sizeof(buf_prev) - sizeof(*hdr) - 1) {
			fprintf(stderr, "invalid link\n");
			return -1;
		}
		linktarget[file.size] = 0;
	}
	return 0;
}

/* skip forward in file, using seek if available, otherwise read and discard */
static int goforward(size_t len) {
	if (have_seek) {
		if (lseek(fd, len, SEEK_CUR) < 0) {
			perror("lseek");
			return -1;
		}
		return 0;
	}
	while (len > 0) {
		size_t bs = len > sizeof(buf_uc) ? sizeof(buf_uc) : len;
		if (mread(fd, &buf_uc, bs) < 0) {
			perror("read");
			return -1;
		}
		len -= bs;
	}
	return 0;
}

static int skipfile() {
	if ((file.mode & S_IFMT) != S_IFREG)
		return 0;
	if (file.flags & FILE_FLAG_UNCOMPRESSED) {
		goforward(file.size + (have_crc ? sizeof(uint64_t) : 0));
	} else {
		for (int i = 0; i < file.nblk; ++i) {
			if (mread(fd, &zd.len, sizeof(zd.len)) < 0) {
				perror("read");
				return -1;
			}
			goforward(zd.len + 1 + (have_crc ? sizeof(uint64_t) : 0));
		}
	}
	return 0;
}

static int extract_regular(int out) {
	if (file.flags & FILE_FLAG_UNCOMPRESSED) {
		size_t len = file.size;
		uint64_t crc = CRCINIT_FILE;
		while (len > 0) {
			size_t bs = sizeof(buf_uc) > len ? len : sizeof(buf_uc);
			if (mread(fd, buf_uc, bs) < 0) {
				perror("read");
				return -1;
			}
			if (opt_crc && have_crc)
				crc = crc64(crc, buf_uc, bs);
			if (write(out, buf_uc, bs) < 0) {
				perror("write");
				return 0;
			}
			len -= bs;
		}
		if (opt_crc && have_crc) {
			uint64_t crc2;
			if (mread(fd, &crc2, sizeof(crc2)) < 0) {
				perror("read");
				return -1;
			}
			if (letoh(&crc2, sizeof(crc2)) != crc) {
				fprintf(stderr, "checksum mismatch\n");
				return -1;
			}
		}
	} else {
		z_reset();
		for (int i = 0; i < file.nblk; ++i) {
			size_t len = z_read(CRCINIT_DATA);
			if (write(out, buf_prev, len) < 0) {
				perror("write");
				return 0;
			}
		}
	}
	return 0;
}

static int extract() {
	uint32_t ifmt = file.mode & S_IFMT;
	if (opt_verbose)
		fprintf(stderr, "%s\n", path);
	if (ifmt == S_IFDIR) {
		if (mkdir(path, file.mode) != 0 && errno != EEXIST) {
			perror("mkdir");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			return -1;
		}
	} else if (ifmt == S_IFIFO) {
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			return -1;
		}
		if (mkfifo(path, file.mode) != 0) {
			perror("mkfifo");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			return -1;
		}
	} else if (ifmt == S_IFLNK) {
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			return -1;
		}
		if (symlink(linktarget, path) != 0) {
			perror("symlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			return -1;
		}
	} else if (ifmt == S_IFREG) {
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		int out = open(path, O_CREAT | O_WRONLY, file.mode);
		int ret = extract_regular(out);
		close(out);
		if (ret < 0)
			return -1;
	} else {
		fprintf(stderr, "unrecognized entry type %d for file '%s'\n",
		        ifmt, path);
		return -1;
	}
	return 0;
}

static int test() {
	uint32_t ifmt = file.mode & S_IFMT;
	if (opt_verbose)
		fprintf(stderr, "%s\n", path);
	if (ifmt != S_IFREG)
		return 0;
	if (file.flags & FILE_FLAG_UNCOMPRESSED) {
		size_t len = file.size;
		uint64_t crc = CRCINIT_FILE;
		while (len > 0) {
			size_t bs = sizeof(buf_uc) > len ? len : sizeof(buf_uc);
			if (mread(fd, buf_uc, bs) < 0) {
				perror("read");
				return -1;
			}
			if (opt_crc && have_crc)
				crc = crc64(crc, buf_uc, bs);
			len -= bs;
		}
		if (opt_crc && have_crc) {
			uint64_t crc2;
			if (mread(fd, &crc2, sizeof(crc2)) < 0) {
				perror("read");
				return -1;
			}
			if (letoh(&crc2, sizeof(crc2)) != crc) {
				fprintf(stderr, "checksum mismatch\n");
				return -1;
			}
		}
	} else {
		for (int i = 0; i < file.nblk; ++i) {
			size_t len;
			if (mread(fd, &zd.len, sizeof(zd.len)) < 0) {
				perror("read");
				return -1;
			}
			len = zd.len + 1;
			size_t rlen = len + (have_crc ? sizeof(uint64_t) : 0);
			if (mread(fd, zd.data, rlen) < 0) {
				perror("read");
				return -1;
			}
			if (opt_crc && have_crc) {
				uint64_t crc = crc64(CRCINIT_DATA, zd.data, len);
				if (letoh(zd.data + len, sizeof(crc)) != crc) {
					fprintf(stderr, "checksum mismatch\n");
					return -1;
				}
			}
		}
	}
	return 0;
}

static int list() {
	printf("%s\n", path);
	if (file.name[0] != 0 && skipfile() < 0) /* no filename means no file */
		return -1;
	return 0;
}

static int print() {
	uint32_t ifmt = file.mode & S_IFMT;
	if (opt_verbose)
		fprintf(stderr, "%s\n", path);
	if (ifmt == S_IFLNK)
		printf("%s\n", linktarget);
	else if (ifmt == S_IFREG && extract_regular(STDOUT_FILENO) < 0)
		return -1;
	return 0;
}

static int nextfile(char *name, uint32_t newlevel) {
	while (level > newlevel) {
		path_dirup(path);
		--level;
	}
	if (path_combine(path, name) < 0) {
		fprintf(stderr, "pathname too long\n");
		return -1;
	}
	++level;
	return 0;
}

// TODO
// using idx makes sense if:
// - listing only
// - processing only part of the files
static int traverse(int (*fn)(), int useidx, int idxonly,
                    int nsub, char **sub) {
	header_t hdr;
	struct stat sb;

	/* read the header */
	if (mread(fd, &hdr, sizeof(hdr)) < 0) {
		perror("read");
		return -1;
	}
	if (betoh(&hdr.magic, sizeof(hdr.magic)) != MAGIC) {
		fprintf(stderr, "magic number incorrect\n");
		return -1;
	}
	if (opt_crc && letoh(&hdr.crc, sizeof(hdr.crc)) !=
	    crc64(CRCINIT_HEAD, &hdr, sizeof(hdr) - sizeof(hdr.crc))) {
		fprintf(stderr, "header checksum failed\n");
		return -1;
	}
	have_crc = !(letoh(&hdr.flags, sizeof(hdr.flags)) & FLAG_NOCRC);
	have_idx = !(letoh(&hdr.flags, sizeof(hdr.flags)) & FLAG_NOIDX);
	have_seek = 0;
	have_seek = fstat(fd, &sb) == 0 && (sb.st_mode & S_IFMT) == S_IFREG;

	/* use the index if we are able and told to do so */
	if (useidx && opt_idx && have_idx && have_seek) {
		/* read the index tail */
		idx_tail_t tail;
		size_t tlen;
		if (lseek(fd, -sizeof(tail), SEEK_END) < 0) {
			perror("read");
			return -1;
		}
		if (mread(fd, &tail, sizeof(tail)) < 0) {
			perror("read");
			return -1;
		}
		if (opt_crc && letoh(&tail.crc, sizeof(tail.crc)) !=
		    crc64(CRCINIT_TAIL, &tail.size, sizeof(tail.size))) {
			fprintf(stderr, "index tail checksum failed\n");
			return -1;
		}
		tlen = letoh(&tail.size, sizeof(tail.size));
		if (lseek(fd, -(sizeof(tail) + tlen), SEEK_END) < 0) {
			perror("read");
			return -1;
		}

		/* read the index */
		size_t idxsz = 0;
		offset = 0; /* z_read() updates this */
		z_reset();
		while (offset < tlen) {
			size_t len = z_read(CRCINIT_IDX);
			if ((idx = realloc(idx, idxsz + len)) == NULL) {
				perror("realloc");
				return -1;
			}
			memcpy((char *) idx + idxsz, buf_prev, len);
			idxsz += len;
		}
		idx_len = idxsz / sizeof(file_idx_t);

		/* iterate over the index */
		uint32_t startlvl = 0, started = nsub ? 0 : 1;
		for (int i = 0; i < idx_len; ++i) {
			if (checkname(idx[i].name, sizeof(idx[i].name)) < 0)
				return -1;
			if (nextfile(idx[i].name, idx[i].level) < 0)
				return -1;

			/* check if this is an we're requested to do deal with */
			if (level <= startlvl)
				started = 0;
			for (int x = 0; x < nsub; ++x) {
				if (strcmp(path, sub[x]) == 0) {
					started = 1;
					startlvl = level;
				} // TODO should we error when entries are not found?
			}
			if (!started) {
				if (skipfile() < 0)
					return -1;
				continue;
			} // TODO either create full path, or more straightforwardly use path relatively

			/* handle the entries when in started state */
			if (!idxonly && lseek(fd, sizeof(header_t) + idx[i].offset,
				SEEK_SET) < 0) {
				perror("read");
				return -1;
			} else file.name[0] = 0; /* to tell fn there's no file */
			if (fn() < 0)
				return -1;
		}

		return 0;
	}

	/* go over all the files */
	uint32_t startlvl = 0, started = nsub ? 0 : 1;
	while (1) { // TODO use index where sensible
		if (readfhdr() < 0)
			return -1;
		if (file.name[0] == 0)
			break; /* no filename indicates end */
		if (nextfile(file.name, file.level) < 0)
			return -1;

		/* check if this is an we're requested to do deal with */
		if (level <= startlvl)
			started = 0;
		for (int x = 0; x < nsub; ++x) {
			if (strcmp(path, sub[x]) == 0) {
				started = 1;
				startlvl = level;
			} // TODO should we error when entries are not found?
		}
		if (!started) {
			if (skipfile() < 0)
				return -1;
			continue;
		}

		if (fn() < 0)
			return -1;
	}
	return 0;
}

static void usage(char *progname) {
	fprintf(stderr, "\n"
	        " ##########################\n"
	        " # mar - The Mat ARchiver #\n"
	        " #########################@\n\n"
	        "  usage: %s [-cxtlwvudTnph] [-f archive] [file] [...]\n\n"
	        "  -c            create archive\n"
	        "  -x            extract archive\n"
	        "  -t            test archive\n"
	        "  -l            list archive contents\n"
	        "  -w            write file contents to standard output\n"
	        "  -v            list files as they are processed\n"
	        "  -u            store files uncompressed\n"
	        "  -d            disable writing/using file index\n"
	        "  -T            allow writing compressed data to tty\n"
	        "  -n            disable CRC creation / verification\n"
	        "  -p            show progress while compressing\n"
	        "  -h            show this message\n"
	        "  -f archive    set archive filename\n"
	        "\n Exactly one of -c, x, t, l or p has to be provided.\n"
	        "\n Copyright (C) 2021 Netman\n\n", progname);
}

#define out_opts (O_WRONLY | O_CREAT | O_TRUNC)
#define out_mode (0644)

enum {
	CMD_NONE,
	CMD_CREATE,
	CMD_EXTRACT,
	CMD_TEST,
	CMD_LIST,
	CMD_PRINT
};

int main(int argc, char *argv[]) {
	int opt, ret = 1, cmd = CMD_NONE;
	char *file = NULL;

	while ((opt = getopt(argc, argv, "cxtlwvudTnphf:")) != -1) {
		switch (opt) {
			case 'c': /* create */
				if (cmd != CMD_NONE)
					goto usagefail;
				cmd = CMD_CREATE;
				break;
			case 'x': /* extract */
				if (cmd != CMD_NONE)
					goto usagefail;
				cmd = CMD_EXTRACT;
				break;
			case 't': /* test */
				if (cmd != CMD_NONE)
					goto usagefail;
				cmd = CMD_TEST;
				break;
			case 'l': /* list */
				if (cmd != CMD_NONE)
					goto usagefail;
				cmd = CMD_LIST;
				break;
			case 'w': /* write file contents to stdout */
				if (cmd != CMD_NONE)
					goto usagefail;
				cmd = CMD_PRINT;
				break;
			case 'v': /* verbose */
				opt_verbose = 1;
				break;
			case 'u': /* uncompressed */
				opt_compress = 0;
				break;
			case 'd': /* disable index */
				opt_idx = 0;
				break;
			case 'T': /* allow writing compressed stuff to tty */
				opt_tty = 1;
				break;
			case 'n': /* do not calculate CRCs */
				opt_crc = 0;
				break;
			case 'p': /* indicate progress */
				opt_progress = 1;
				break;
			case 'h': /* show help */
				usage(argv[0]);
				ret = 0;
				goto quit;
			case 'f': /* specify archive file */
				if (file != NULL) /* prevent multiple -o */
					goto usagefail;
				file = optarg;
				break;
			default:
				goto usagefail;
		}
	}

	if (cmd == CMD_NONE)
		goto usagefail;

	if (cmd == CMD_CREATE) {
		fd = STDOUT_FILENO;
		if (file != NULL && (fd = open(file, out_opts, out_mode)) < 0) {
			perror("open");
			goto quit;
		}
	} else {
		fd = STDIN_FILENO;
		if (file != NULL && (fd = open(file, O_RDONLY)) < 0) {
			perror("open");
			goto quit;
		}
	}

	switch (cmd) {
		case CMD_CREATE:
			ret = create(argc - optind, argv + optind); // TODO on error return 1 not -1
			break;
		case CMD_EXTRACT: // TODO using index where can
			ret = traverse(extract, 0, 0, argc - optind, argv + optind);
			break;
		case CMD_TEST:
			ret = traverse(test, 0, 0, argc - optind, argv + optind);
			break;
		case CMD_LIST:
			ret = traverse(list, 1, 1, argc - optind, argv + optind);
			break;
		case CMD_PRINT:
			ret = traverse(print, 0, 0, argc - optind, argv + optind);
			break;
	}
	free(idx);

quit:
	if (fd >= 0 && fd != STDIN_FILENO && fd != STDOUT_FILENO)
		close(fd);
	return ret;

usagefail:
	usage(argv[0]);
	goto quit;
}
