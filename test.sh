#!/bin/mksh
cd tests
for f in *; do
	echo
	wc -c "$f"
	echo -n "mz  "
	(time ../mz -n "$f") | wc -c
	echo -n "lz4 "
	(time lz4 -BD "$f" -c) | wc -c
done
