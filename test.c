#include <stdio.h>

short map[0x10000];
char buf_in[0x10000] = "12345aaaaaaaaaa";
char buf_out[0x10000];

extern int mz_compress(short *map, char *out, char *in, int len, char *prev, int prev_len);

int main() {
	int ret = mz_compress(map, buf_out, buf_in, sizeof(buf_in), (void *) 0, 0);
	printf("%08X\n", ret);
	return 0;
}
