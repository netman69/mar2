#!/bin/mksh
cd dtests
for f in *; do
	echo
	wc -c "$f"
	echo -n "mz  "
	(time ../mz -d "$f") | wc -c
	echo -n "lz4 "
	(time lz4 -dc ../dtests_lz4/"$f".lz4) | wc -c
done
