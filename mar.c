/* mar - The Mat ARchiver.
 * 
 * Simple utility similar to tar with the following features:
 * - Store filename, mode, not uid/gid/etc.
 * - Option to extract everything except given filenames.
 * - Compression with MatZip (on a per-file basis, no need to extract entire
 *     archive to retrieve a single file).
 * - CRC64 hashes to verify archive integrity.
 * - Extract files separately without reading the entire archive.
 * - Downsides of the design are relatively weak compression (but fast), and
 *     archives can't be passed through stdin/stdout because seeking is done.
 * 
 * TODO
 * - maybe move the header to the end so we can stream to stdout, but then
 *     streaming from stdin becomes entirely impossible
 * - maybe have separate file headers prefixed to the file, skip crc+sz+idxoff
 *     in header, and only use index when not streaming (idx at the end)
 * - do compress ALL the file headers even if we prefix files with them
 * - we'd have to have compression as a global flag
 * - have a minimum size treshold for compressing
 * - maybe condense all the endian conversion into one place
 * - progress display a la mz utility
 * - documentation documentation documentation
 * - install target for makefile
 * - document file format, how traversing deals with . and .. (treats the folder as root)
 * - use uint16_t for block length prefixes
 */

#define _XOPEN_SOURCE 500 /* nftw() */
#include <ftw.h> /* nftw() */
#include <sys/types.h> /* mkfifo() */
#include <sys/stat.h> /* mkdir(), fstat(), mkfifo() */
#include <stdint.h> /* uint8_t, uint32_t, uint64_t */
#include <stdlib.h> /* malloc(), realloc(), exit() */
#include <stddef.h> /* NULL, size_t */
#include <string.h> /* strncpy(), strcmp(), strlen(), rindex() */
#include <stdio.h> /* printf(), perror(), fprintf(), stderr, stdout */
#include <limits.h> /* PATH_MAX */
#include <fcntl.h> /* open() */
#include <unistd.h> /* read(), write(), close(), symlink(), lseek() */
#include <errno.h> /* errno, EEXIST */
#include "mz.h"
#include "crc64.h"
#include "end.h"

#define MAR_MAGIC (0x455649484352414d) // TODO why not write this as big endian
#define MAR_BUFSZ (65536)

typedef struct __attribute__((__packed__)) {
	uint64_t magic;
	uint64_t nfiles; /* includes directories, links, etc */
	uint64_t icrc; /* CRC of header + index with icrc set to 0 */
	uint64_t size; /* size of entire file */
	uint64_t idxoffset;
	uint32_t flags;
	uint8_t reserved[20];
} mar_t;

#define MAR_HDRFLAG_IDXCOMPRESSED (1 << 0)

typedef struct __attribute__((__packed__)) {
	char name[256];
	uint32_t mode;
	uint64_t size; /* as in archive, compressed data has compressed size */
	uint64_t offset; /* relative to start of file */
	uint32_t level; /* 0 is the root of the package */
	uint64_t crc;
	uint32_t flags;
	uint8_t reserved[28];
} file_t;

#define MAR_FLAG_DONTEXTRACT (1 << 0) /* only ever set during extraction */
#define MAR_FLAG_COMPRESSED (1 << 1)

struct __attribute__((__packed__)) {
	uint32_t size; // TODO should we use uint16_t? should 0 be 1? yes!
	uint8_t buf[MZ_OUTPUTMAX(MAR_BUFSZ)];
} mz_data;

uint64_t nfiles = 0;
file_t *files = NULL;
char **paths = NULL;
uint64_t offset;
int fd, bufno = 0;
uint8_t buf[2][MAR_BUFSZ];
mz_map_t mz_map;

int verbose = 0;
int compress = 1;
int compress_index = 1;
int verify = 1; /* don't calculate CRC for files to extract faster */
int ignorecrc = 0; /* ignore CRC errors (still report them if verify is on) */

ssize_t mread(int fd, void *buf, size_t count) {
	while (count) {
		ssize_t r = read(fd, buf, count);
		if (r == 0) {
			errno = EIO;
			return -1;
		}
		if (r < 0)
			return -1;
		buf = ((char *) buf) + r;
		count -= r;
	}
	return 0;
}

int addfile_dots;

static int addfile(const char *fpath, const struct stat *sb, int tflag,
	struct FTW *ftwbuf) {
	uint64_t f;
	uint32_t ifmt;
	if (!addfile_dots && strcmp(fpath + ftwbuf->base, "..") == 0) {
		fprintf(stderr, "warning: removing .. from in-archive path\n");
		addfile_dots = 1;
		return 0;
	}
	if (!addfile_dots && (strcmp(fpath + ftwbuf->base, ".") == 0 ||
		strcmp(fpath + ftwbuf->base, "") == 0)) {
		addfile_dots = 1;
		return 0;
	}
	f = nfiles++;
	if ((files = realloc(files, nfiles * sizeof(file_t))) == NULL) {
		perror("realloc");
		exit(1);
	}
	if ((paths = realloc(paths, nfiles * sizeof(char *))) == NULL) {
		perror("realloc");
		exit(1);
	}
	strncpy(files[f].name, fpath + ftwbuf->base, sizeof(files[f].name) - 1);
	if ((paths[f] = strdup(fpath)) == NULL) {
		perror("strdup");
		exit(1);
	}
	files[f].name[sizeof(files[f].name) - 1] = 0;
	htole(&files[f].level, ftwbuf->level - addfile_dots, 4);
	htole(&files[f].mode, sb->st_mode, 4);
	ifmt = sb->st_mode & S_IFMT;
	if (ifmt == S_IFREG || ifmt == S_IFLNK)
		htole(&files[f].size, sb->st_size, 8);
	else {
		files[f].size = 0;
		files[f].offset = 0;
	}
	return 0;
}

void dirup(char *path) {
	int end = strlen(path);
	if (end == 0)
		return;
	while (--end) {
		if (path[end] == '/')
			break;
	}
	path[end] = 0;
}

void traverse(void (*fn)(const char *path, file_t *file, int n),
	const char *where) {
	char path[PATH_MAX + 1] = "";
	char *rpath = path;
	int lvl = 0, i;
	int here = ((where == NULL) ? -1 : -2);
	for (i = 0; i < nfiles; ++i) {
		if (files[i].name[0] == 0 || strchr(files[i].name, '/') ||
			strcmp(files[i].name, "..") == 0) {
			fprintf(stderr, "something is iffy about this archive, stopped\n");
			exit(1);
		}
		while (lvl > 0 && letoh(&files[i].level, 4) <= lvl) {
			dirup(path);
			--lvl;
		}
		lvl = letoh(&files[i].level, 4);
		if (lvl == 0) {
			dirup(path);
		} else {
			strncat(path, "/", sizeof(path) - 1);
			path[sizeof(path) - 1] = 0;
		}
		strncat(path, files[i].name, sizeof(path) - 1);
		path[sizeof(path) - 1] = 0;
		if (here < -1) {
			if (strcmp(path, where) == 0) {
				here = lvl;
				rpath = strrchr(path, '/');
				rpath = (rpath == NULL) ? path : rpath + 1;
				fn(rpath, files + i, i);
			}
		} else {
			if (lvl <= here)
				return;
			fn(rpath, files + i, i);
		}
	}
	if (here < -1) {
		fprintf(stderr, "no such file or directory in archive\n");
		exit(1);
	}
}

static void package(const char *path, file_t *file, int n) {
	uint32_t ifmt;
	if (verbose)
		printf("%s\n", path);
	ifmt = letoh(&file->mode, 4) & S_IFMT;
	if (ifmt == S_IFLNK) {
		char target[PATH_MAX + 1];
		ssize_t len = readlink(paths[n], target, sizeof(target) - 1);
		if (len < 0) {
			perror("readlink");
			fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
			exit(1);
		}
		target[len] = 0;
		if (write(fd, target, len) != len) {
			perror("write");
			fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
			exit(1);
		}
		htole(&file->crc, crc64(CRC64_INIT, target, len), 8);
		htole(&file->offset, offset, 8);
		offset += len;
	} else if (ifmt == S_IFREG) {
		uint64_t size = letoh(&file->size, 8), csz_tot = 0, crc;
		int in = open(paths[n], O_RDONLY);
		if (in < 0) {
			perror("open");
			fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
			exit(1);
		}

		crc = CRC64_INIT;
		if (compress) {
			size_t buf_prevsz = 0;
			while (size > 0) {
				size_t bs = sizeof(buf[0]) > size ? size : sizeof(buf[0]), csz;
				if (mread(in, buf[bufno], bs) < 0) {
					perror("read");
					fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
					exit(1);
				}
				csz = mz_compress(&mz_map, mz_data.buf, buf[bufno], bs,
				                  buf[(bufno + 1) % 2], buf_prevsz);
				buf_prevsz = bs;
				htole(&mz_data.size, csz, 4);
				csz += 4;
				crc = crc64(crc, &mz_data, csz);
				if (write(fd, &mz_data, csz) != csz) {
					perror("write");
					fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
					exit(1);
				}
				csz_tot += csz;
				bufno = (bufno + 1) % 2;
				size -= bs;
			}
			htole(&file->offset, offset, 8);
			uint32_t flag;
			htole(&flag, MAR_FLAG_COMPRESSED, 4);
			file->flags |= flag;
			htole(&file->size, csz_tot, 8);
			offset += csz_tot;
		} else {
			while (size > 0) {
				int bs = sizeof(buf) > size ? size : sizeof(buf);
				if (mread(in, buf, bs) < 0) {
					perror("read");
					fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
					exit(1);
				}
				crc = crc64(crc, buf, bs);
				if (write(fd, buf, bs) != bs) {
					perror("write");
					fprintf(stderr, "\twhile adding '%s'\n", paths[n]);
					exit(1);
				}
				size -= bs;
			}
			htole(&file->offset, offset, 8);
			offset += letoh(&file->size, 8);
		}

		htole(&file->crc, crc, 8);
		close(in);
	} else if (ifmt != S_IFDIR && ifmt != S_IFIFO) {
		fprintf(stderr, "can't add special file '%s'\n", paths[n]);
		exit(1);
	}
}

static uint64_t extract_regular(int out, uint64_t size, int compressed,
                                const char *path) {
	uint64_t crc = CRC64_INIT;
	if (compressed) {
		uint32_t csz, tsz = 0;
		size_t buf_prevsz = 0;
		int dsz;
		while (size > tsz) {
			if (mread(fd, &mz_data.size, 4) < 0) {
				perror("read");
				fprintf(stderr, "\twhile extracting '%s'\n", path);
				exit(1);
			}
			csz = letoh(&mz_data.size, 4);
			if (mread(fd, mz_data.buf, csz) < 0) {
				perror("read");
				fprintf(stderr, "\twhile extracting '%s'\n", path);
				exit(1);
			}
			if (verify)
				crc = crc64(crc, &mz_data, 4 + csz);
			dsz = mz_decompress(buf[bufno], sizeof(buf[0]),
			                    buf[(bufno + 1) % 2], buf_prevsz,
			                    mz_data.buf, csz);
			buf_prevsz = dsz;
			if (write(out, buf[bufno], dsz) != dsz) {
				perror("write");
				fprintf(stderr, "\twhile extracting '%s'\n", path);
				exit(1);
			}
			bufno = (bufno + 1) % 2;
			tsz += 4 + csz;
		}
	} else {
		while (size > 0) {
			int bs = sizeof(buf) > size ? size : sizeof(buf);
			if (mread(fd, buf, bs) < 0) {
				perror("read");
				fprintf(stderr, "\twhile extracting '%s'\n", path);
				exit(1);
			}
			if (verify)
				crc = crc64(crc, buf, bs);
			if (write(out, buf, bs) != bs) {
				perror("write");
				fprintf(stderr, "\twhile extracting '%s'\n", path);
				exit(1);
			}
			size -= bs;
		}
	}
	return crc;
}

static void extract(const char *path, file_t *file, int n) {
	uint32_t ifmt;
	if (letoh(&file->flags, 4) & MAR_FLAG_DONTEXTRACT)
		return;
	if (verbose)
		printf("%s\n", path);
	ifmt = letoh(&file->mode, 4) & S_IFMT;
	if (ifmt == S_IFDIR) {
		if (mkdir(path, letoh(&file->mode, 4)) != 0 && errno != EEXIST) {
			perror("mkdir");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
	} else if (ifmt == S_IFIFO) {
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		if (mkfifo(path, letoh(&file->mode, 4)) != 0) {
			perror("mkfifo");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
	} else if (ifmt == S_IFLNK) {
		char target[PATH_MAX + 1];
		uint64_t len = letoh(&file->size, 8);
		if (len > sizeof(target) - 1) {
			fprintf(stderr, "symlink too long\n");
			exit(1);
		}
		if (lseek(fd, letoh(&file->offset, 8), SEEK_SET) < 0) {
			perror("lseek");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		if (mread(fd, target, len) < 0) {
			perror("read");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		target[len] = 0;
		if (letoh(&file->crc, 8) != crc64(CRC64_INIT, target, len)) {
			fprintf(stderr, "symlink '%s' is corrupted\n", path);
			if (!ignorecrc)
				exit(1);
		}
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		if (symlink(target, path) != 0) {
			perror("symlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
	} else if (ifmt == S_IFREG) {
		uint64_t size = letoh(&file->size, 8), crc;
		int out, compressed = letoh(&file->flags, 4) & MAR_FLAG_COMPRESSED;
		if (unlink(path) != 0 && errno != ENOENT) {
			perror("unlink");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		out = open(path, O_CREAT | O_WRONLY, letoh(&file->mode, 4));
		if (lseek(fd, letoh(&file->offset, 8), SEEK_SET) < 0) {
			perror("lseek");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		if (out < 0) {
			perror("open");
			fprintf(stderr, "\twhile extracting '%s'\n", path);
			exit(1);
		}
		crc = extract_regular(out, size, compressed, path);
		if (verify && letoh(&file->crc, 8) != crc) {
			fprintf(stderr, "file '%s' is corrupted\n", path);
			if (!ignorecrc)
				exit(1);
		}
		close(out);
	}
}

static void test(const char *path, file_t *file, int n) {
	uint32_t ifmt;
	if (verbose)
		printf("%s\n", path);
	ifmt = letoh(&file->mode, 4) & S_IFMT;
	if (ifmt == S_IFREG || ifmt == S_IFLNK) {
		uint64_t size = letoh(&file->size, 8), crc = CRC64_INIT;
		if (lseek(fd, letoh(&file->offset, 8), SEEK_SET) < 0) {
			perror("lseek");
			fprintf(stderr, "\twhile testing '%s'\n", path);
			exit(1);
		}
		while (size > 0) {
			int bs = sizeof(buf) > size ? size : sizeof(buf);
			if (mread(fd, buf, bs) < 0) {
				perror("read");
				fprintf(stderr, "\twhile testing '%s'\n", path);
				exit(1);
			}
			crc = crc64(crc, buf, bs);
			size -= bs;
		}
		if (letoh(&file->crc, 8) != crc) {
			fprintf(stderr, "file/symlink '%s' is corrupted\n", path);
			exit(1);
		}
	}
}

static void list(const char *path, file_t *file, int n) {
	printf("%s\n", path);
}

static void print(const char *path, file_t *file, int n) {
	uint64_t size = letoh(&file->size, 8), crc;
	int compressed = letoh(&file->flags, 4) & MAR_FLAG_COMPRESSED;
	if ((letoh(&file->mode, 4) & S_IFMT) != S_IFREG)
		return;
	if (lseek(fd, letoh(&file->offset, 8), SEEK_SET) < 0) {
		perror("lseek");
		fprintf(stderr, "\twhile printing '%s'\n", path);
		exit(1);
	}
	crc = extract_regular(STDOUT_FILENO, size, compressed, path);
	if (verify && letoh(&file->crc, 8) != crc) {
		fprintf(stderr, "file '%s' is corrupted\n", path);
		if (!ignorecrc)
			exit(1);
	}
}

static void dontextract(const char *path, file_t *file, int n) {
	uint32_t flag;
	htole(&flag, MAR_FLAG_DONTEXTRACT, 4);
	file->flags |= flag;
}

void openmar(const char *path) {
	mar_t mar;
	uint64_t icrc;
	struct stat sb;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		perror("open");
		exit(1);
	}

	if (mread(fd, &mar, sizeof(mar)) < 0) {
		perror("read");
		exit(1);
	}

	/* verify header */
	if (letoh(&mar.magic, 8) != MAR_MAGIC) {
		fprintf(stderr, "not a valid mar archive, magic mismatches\n");
		exit(1);
	}
	icrc = letoh(&mar.icrc, 8);
	mar.icrc = 0;
	mar.icrc = crc64(CRC64_INIT, &mar, sizeof(mar));

	/* read index */
	if (lseek(fd, mar.idxoffset, SEEK_SET) < 0) {
		perror("lseek");
		exit(1);
	}
	nfiles = letoh(&mar.nfiles, 8);
	files = malloc(nfiles * sizeof(file_t));
	if (letoh(&mar.flags, 4) & MAR_HDRFLAG_IDXCOMPRESSED) {
		uint64_t idxbsz = letoh(&mar.size, 8) - letoh(&mar.idxoffset, 8);
		char *idxb = malloc(idxbsz);
		if (idxb == NULL) {
			perror("malloc");
			exit(1);
		}
		if (mread(fd, idxb, idxbsz) < 0) {
			perror("read");
			exit(1);
		}
		mz_decompress((void *) files, sizeof(file_t) * nfiles, NULL, 0,
		              (void *)idxb, idxbsz);
		mar.icrc = crc64(mar.icrc, idxb, idxbsz);
	} else {
		if (mread(fd, files, sizeof(file_t) * nfiles) < 0) {
			perror("read");
			exit(1);
		}
		mar.icrc = crc64(mar.icrc, files, sizeof(file_t) * nfiles);
	}

	/* verify header and index CRC, check length */
	if (icrc != mar.icrc) {
		fprintf(stderr, "icrc mismatch\n");
		if (!ignorecrc)
			exit(1);
	}
	if (fstat(fd, &sb) != 0) {
		perror("fstat");
		exit(1);
	}
	if (sb.st_size != letoh(&mar.size, 8)) {
		fprintf(stderr, "archive size mismatch\n");
		if (!ignorecrc)
			exit(1);
	}
}

void usage(const char *pname) {
	fprintf(stderr, "usage: %s {c|x|t|l|p|e}[vunid] <archive> [file] [...]\n",
	        pname);
}

enum {
	CMD_NONE,
	CMD_CREATE,
	CMD_EXTRACT,
	CMD_TEST,
	CMD_LIST,
	CMD_PRINT,
	CMD_EXCEPT
};

int main(int argc, char *argv[]) {
	int i;

	if (argc < 3) {
		nope:
		usage(argv[0]);
		return 1;
	}

	int cmd = CMD_NONE;
	char *c = argv[1];
	while (*c) switch (*c++) {
		case 'c':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_CREATE;
			break;
		case 'x':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_EXTRACT;
			break;
		case 't':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_TEST;
			break;
		case 'l':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_LIST;
			break;
		case 'p':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_PRINT;
			break;
		case 'e':
			if (cmd != CMD_NONE)
				goto nope;
			cmd = CMD_EXCEPT;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'u':
			compress = 0;
			break;
		case 'd':
			compress_index = 0;
		case 'n':
			verify = 0;
			break;
		case 'i':
			ignorecrc = 1;
			break;
		default:
			goto nope;
	}

	if (cmd == CMD_CREATE) { /* create archive */
		mar_t mar = { .magic = 0, .nfiles = 0, .icrc = 0 };
		htole(&mar.magic, MAR_MAGIC, 8);
		char *idxb = NULL; /* initialized to avoid GCC warning */
		int idxbsz, idxlen;
		fd = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (fd < 0) {
			perror("open");
			return 1;
		}

		for (i = 3; i < argc; ++i) {
			addfile_dots = 0;
			if (nftw(argv[i], addfile, 99, FTW_PHYS) == -1) {
				perror("nftw");
				return 1;
			}
		}

		/* write files (updates index and offset) */
		offset = sizeof(mar_t);
		if (lseek(fd, offset, SEEK_SET) < 0) {
			perror("lseek");
			return 1;
		}
		traverse(package, NULL);

		/* write index */
		idxlen = sizeof(file_t) * nfiles;
		mar.idxoffset = offset;
		if (compress_index) {
			idxbsz = MZ_OUTPUTMAX(idxlen);
			idxb = malloc(idxbsz);
			if (idxb == NULL) {
				perror("malloc");
				return 1;
			}
			idxlen = mz_compress(&mz_map, (void *) idxb, (void *) files,
			                     idxlen, NULL, 0);
			if (write(fd, idxb, idxlen) != idxlen) {
				perror("write");
				return 1;
			}
			uint32_t flag;
			htole(&flag, MAR_HDRFLAG_IDXCOMPRESSED, 4);
			mar.flags |= flag;
		} else {
			if (write(fd, files, idxlen) != idxlen) {
				perror("write");
				return 1;
			}
		}

		/* write header */
		htole(&mar.nfiles, nfiles, 8);
		htole(&mar.size, offset + idxlen, 8);
		mar.icrc = 0;
		mar.icrc = crc64(CRC64_INIT, &mar, sizeof(mar));
		if (compress_index)
			htole(&mar.icrc, crc64(mar.icrc, idxb, idxlen), 8);
		else htole(&mar.icrc, crc64(mar.icrc, files, idxlen), 8);
		if (lseek(fd, 0, SEEK_SET) < 0) {
			perror("lseek");
			return 1;
		}
		if (write(fd, &mar, sizeof(mar)) != sizeof(mar)) {
			perror("write");
			return 1;
		}

		close(fd);
	} else if (cmd == CMD_EXTRACT) { /* extract archive contents */
		openmar(argv[2]);
		if (argc > 3) {
			for (i = 3; i < argc; ++i)
				traverse(extract, argv[i]);
		} else traverse(extract, NULL);
		close(fd);
	} else if (cmd == CMD_TEST) { /* test archive */
		openmar(argv[2]);
		if (argc > 3) {
			for (i = 3; i < argc; ++i)
				traverse(test, argv[i]);
		} else traverse(test, NULL);
		close(fd);
	} else if (cmd == CMD_LIST) { /* list archive contents */
		openmar(argv[2]);
		close(fd);
		if (argc > 3) {
			for (i = 3; i < argc; ++i)
				traverse(list, argv[i]);
		} else traverse(list, NULL);
	} else if (cmd == CMD_PRINT) { /* print files to stdout */
		openmar(argv[2]);
		if (argc > 3) {
			for (i = 3; i < argc; ++i)
				traverse(print, argv[i]);
		} else traverse(print, NULL);
		close(fd);
	} else if (cmd == CMD_EXCEPT) { /* extract archive except ... */
		openmar(argv[2]);
		for (i = 3; i < argc; ++i)
			traverse(dontextract, argv[i]);
		traverse(extract, NULL);
		close(fd);
	} else goto nope;

	return 0;
}
