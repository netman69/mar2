#ifndef __TIME64_H__
#define __TIME64_H__

#include <sys/time.h> /* gettimeofday() */
#include <stdint.h> /* uint64_t */

inline static uint64_t time64() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t) tv.tv_usec + (uint64_t) tv.tv_sec * 1000000;
}

#endif /* __TIME64_H__ */
