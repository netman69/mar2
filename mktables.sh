#!/bin/sh
set -e

# Print one array.
printsub() {
	echo "	{"
	./tablegen "		" 3 $1
	echo "	},"
}

# Proceed.
echo "#include <stdint.h>"
echo "#include \"tables.h\""
echo
echo "const uint64_t crc64_tbl[16][256] = {"
for i in $(seq 0 15); do
	printsub "$i"
done
echo "};"
echo
echo "#ifndef MATCRC_NO_COMBINE"
echo "const uint64_t crc64_tbl_combine[59][256] = {"

# Below was generated with:
#   for i in $(seq 5 63); do echo printsub $(((1 << i) - 8)); done
# I don't think that'd work on every shell hence it's put here static.
printsub 24
printsub 56
printsub 120
printsub 248
printsub 504
printsub 1016
printsub 2040
printsub 4088
printsub 8184
printsub 16376
printsub 32760
printsub 65528
printsub 131064
printsub 262136
printsub 524280
printsub 1048568
printsub 2097144
printsub 4194296
printsub 8388600
printsub 16777208
printsub 33554424
printsub 67108856
printsub 134217720
printsub 268435448
printsub 536870904
printsub 1073741816
printsub 2147483640
printsub 4294967288
printsub 8589934584
printsub 17179869176
printsub 34359738360
printsub 68719476728
printsub 137438953464
printsub 274877906936
printsub 549755813880
printsub 1099511627768
printsub 2199023255544
printsub 4398046511096
printsub 8796093022200
printsub 17592186044408
printsub 35184372088824
printsub 70368744177656
printsub 140737488355320
printsub 281474976710648
printsub 562949953421304
printsub 1125899906842616
printsub 2251799813685240
printsub 4503599627370488
printsub 9007199254740984
printsub 18014398509481976
printsub 36028797018963960
printsub 72057594037927928
printsub 144115188075855864
printsub 288230376151711736
printsub 576460752303423480
printsub 1152921504606846968
printsub 2305843009213693944
printsub 4611686018427387896
printsub 9223372036854775800

echo "};"
echo "#endif"
