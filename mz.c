#include <stdint.h>
#include <string.h> /* memcpy() */
#include "mz.h"

/* TODO
 * - literal and match lengths have some possible values that are invalid
 *     we could use those for something extra perhaps
 * - LZ4 uses 5 byte hashes, which seems to work better for some data, do we
 *     want to copy that idea?
 */

#define read64(p) (*(uint64_t *) (p))
#define read32(p) (*(uint32_t *) (p))
#define read16(p) (*(uint16_t *) (p))

#define write64(d, s) (*(uint64_t *) (d) = (s))
#define write32(d, s) (*(uint32_t *) (d) = (s))
#define write16(d, s) (*(uint16_t *) (d) = (s))

#ifdef __GNUC__ /* to ensure short memcpy inlined even with -ffreestanding */
	#define memcpy __builtin_memcpy
#endif

/* hash function used for the hashmap to find 4-byte string matches */
inline static uint16_t mz_hash4(uint32_t x) {
	//return ((uint64_t) x * 2654435761U) >> 22;
	//return (x * 2654435761U) >> 16;
	//return (x & 0xFFFF) ^ (x >> 16);
	return ((x << 19) - x) >> 16; /* knuth hash with mersenne prime */
}

/* fill hashmap with data from dictionary */
void mz_loaddict(mz_map_t *map, uint8_t *in, uint32_t in_len) {
	for (uint32_t i = 0; i + 4 <= in_len; ++i)
		(*map)[mz_hash4(read32(in + i))] = i;
}

/* memcpy that is faster for short bits, but accesses at least 16 bytes */
inline static void mz_memcpy16(uint8_t *out, uint8_t *in, uint32_t len) {
	if (len <= 16) {
		memcpy(out, in, 16);
	} else memcpy(out, in, len);
	/* Alternatively we could overread+overwrite 15 extra bytes always, this is
	 *   fast but not quite as fast by my testing, and harder to account for:
	 * uint8_t *out_end = out + len;
	 * do {
	 * 	memcpy(out, in, 16);
	 * 	out += 16, in += 16;
	 * } while (out < out_end);
	 */
}

/* output a sequence */
inline static uint8_t *mz_sequence(uint8_t *out, uint8_t *ls, uint32_t ll,
                                   uint8_t *in_end, uint32_t match,
                                   uint32_t ml) {
	uint8_t *token = out++;

	/* encode literal length */
	uint32_t llc = ll;
	if (llc < 15) { /* 0-14 are encoded in the token */
		*token = llc << 4;
	} else {
		*token = 15 << 4;
		if (llc < 255 + 15) { /* 15-269 are encoded with one extra byte */
			*out++ = llc - 15;
		} else { /* 270 and up are encoded with two extra bytes */
			llc -= 270;
			*out++ = 255;
			*out++ = llc; /* hmm, we don't need all possible values (yet) */
			*out++ = llc >> 8;
		}
	}

	/* write literal data */
	if (in_end - ls >= 16)
		mz_memcpy16(out, ls, ll);
	else memcpy(out, ls, ll);
	out += ll;

	/* quit if we have no match part (end of compressed block) */
	if (!ml)
		return out;

	/* encode match length */
	ml -= 4;
	if (ml < 15) { /* 4-18 are encoded in the token */
		*token |= ml;
	} else {
		*token |= 15;
		if (ml < 255 + 15) { /* 19-273 encode with one extra byte */
			*out++ = ml - 15;
		} else { /* 274 and up are encoded with two extra bytes */
			ml -= 270;
			*out++ = 255;
			*out++ = ml;
			*out++ = ml >> 8;
		}
	}

	/* encode match offset */
	--match; /* range is 1-0x10000 */
	*out++ = match;
	*out++ = match >> 8;

	return out;
}

/* count leading zeros in a string cast to uint64_t (up to 7) */
inline static uint32_t mz_count56(uint64_t x) {
	if (*(uint16_t *) "\0\xff" > 0x100) { /* hopefully optimized away */
		/* little endian */
		#ifdef __GNUC__
			return __builtin_ctzll(x) >> 3;
		#else
			uint64_t m = 0x0101010101010101ULL;
			return (((x ^ (x - 1)) & (m - 1)) * m) >> 56;
		#endif
	} else {
		/* big endian */
		#ifdef __GNUC__
			return __builtin_clzll(x) >> 3;
		#else
			uint32_t n = 7;
			if (x & 0xFFFFFFFF00000000) n ^= 4;
			if (x & 0xFFFF0000FFFF0000) n ^= 2;
			if (x & 0xFF00FF00FF00FF00) n ^= 1;
			return n;
		#endif
	}
}

/* count how long a match is, first 4 bytes are assumed to match */
inline static uint32_t mz_count(uint8_t *in, uint8_t *match, uint32_t ml_max) {
	uint32_t ml = 4, ec = (ml_max - 4) >> 3;
	while (ec--) {
		uint64_t diff = read64(in + ml) ^ read64(match + ml);
		if (diff)
			return ml + mz_count56(diff);
		ml += 8;
	}
	if (ml + 4 <= ml_max && read32(in + ml) == read32(match + ml))
		ml += 4;
	if (ml + 2 <= ml_max && read16(in + ml) == read16(match + ml))
		ml += 2;
	if (ml < ml_max && *(in + ml) == *(match + ml))
		++ml;
	return ml;
}

/* compress a block of data */
uint32_t mz_compress(mz_map_t *map, uint8_t *out, uint8_t *in, uint32_t in_len,
                     uint8_t *prev, uint32_t prev_len) {
	uint8_t *out_start = out, *in_start = in, *in_end = in + in_len;

	while (1) {
		uint32_t match, ml; /* match is uint32_t to avoid rollover */
		uint8_t *ls = in; /* if there's a literal, this is where it starts */

		/* go through input until we find a match */
		while (in + 4 <= in_end) {
			//uint16_t hash = mz_hash4(read32(in) ^ *(in + 4));
			uint16_t hash = mz_hash4(read32(in));
			match = (*map)[hash];
			(*map)[hash] = in - in_start;
			if (match < in - in_start &&
			    read32(in) == read32(in_start + match)) {
				ml = mz_count(in, in_start + match, in_end - in);
				match = in - (in_start + match);
				break;
			}
			if (match + 4 <= prev_len &&
			    in - in_start + (prev_len - match) <= 0x10000 &&
			    read32(in) == read32(prev + match)) {
				uint32_t max = prev_len - match;
				if (max > in_end - in)
					max = in_end - in;
				ml = mz_count(in, prev + match, max);
				match = in - in_start + (prev_len - match);
				break;
			}
			//++in;
			in += 1 + ((in - ls) >> 4); /* skip incompressible data */
		}
		if (in + 4 > in_end) {
			if (ls < in_end)
				out = mz_sequence(out, ls, in_end - ls, in_end, 0, 0);
			return out - out_start;
		}
		/* hash matched part, slows down compressible data severely */
		//for (uint8_t *hs = in + 1; hs < in + ml && hs < in_end - 3; ++hs)
		//	(*map)[mz_hash4(read32(hs))] = hs - in_start;
		out = mz_sequence(out, ls, in - ls, in_end, match, ml);
		in += ml;
	}
}

/* copy data from left of the input pointer, overlapping makes patterns */
inline static void mz_offcpy(uint8_t *out, uint32_t off, uint32_t len) {
	uint8_t *out_end = out + len, *in = out - off;
	if (off < 16) {
		uint8_t *tgt = out + off + 16;
		while (out < tgt && out < out_end)
			*out++ = *in++;
		while (out - in < 16)
			in -= off;
	}
	while (out_end - out >= 16)
		memcpy(out, in, 16), out += 16, in += 16;
	if (out_end - out >= 8)
		write64(out, read64(in)), out += 8, in += 8;
	if (out_end - out >= 4)
		write32(out, read64(in)), out += 4, in += 4;
	if (out_end - out >= 2)
		write16(out, read16(in)), out += 2, in += 2;
	if (out < out_end)
		*out = *in;
}

/* decompress a block of data */
uint32_t mz_decompress(uint8_t *out, uint32_t out_len,
                       uint8_t *prev, uint32_t prev_len,
                       uint8_t *in, uint32_t in_len) {
	uint8_t *out_start = out, *out_end = out + out_len, *in_end = in + in_len;

	while (in < in_end) {
		/* read a token and extra literal length bytes */
		uint8_t token = *in++; /* the loop ensures a byte is available */
		uint32_t ll = token >> 4;
		if (ll == 15 && in_end - in >= 3) {
			/* range check done combined, 3 bytes must exist for data anyway */
			ll += *in++;
			if (ll == 270) {
				ll += *in++;
				ll += *in++ << 8;
			}
		}

		/* output the literal */
		if (ll > in_end - in || ll > out_end - out)
			break;
		if (out_end - out >= 16 && in_end - in >= 16)
			mz_memcpy16(out, in, ll);
		else memcpy(out, in, ll);
		out += ll;
		in += ll;

		/* read match length */
		uint32_t ml = (token & 0x0F) + 4;
		if (ml == 19 && in_end - in >= 1) {
			ml += *in++;
			if (ml == 274 && in_end - in >= 2) {
				ml += *in++;
				ml += *in++ << 8;
			}
		}

		/* read the match offset */
		uint32_t match;
		if (in_end - in < 2)
			break;
		match = *in++;
		match |= *in++ << 8;
		++match;

		/* copy the match to the output (from earlier in the output) */
		if (match > out - out_start) {
			match -= out - out_start;
			if (match > prev_len || prev_len - match + ml > prev_len || 
			    ml > out_end - out)
				break;
			if (out_end - out >= 16 && prev_len - match >= 16)
				mz_memcpy16(out, prev + prev_len - match, ml);
			else memcpy(out, prev + prev_len - match, ml);
		} else {
			if (match > out - out_start || ml > out_end - out)
				break;
			if (match < ml) /* we can't use memcpy for overlapping data */
				mz_offcpy(out, match, ml);
			else {
				if (out_end - out >= 16)
					mz_memcpy16(out, out - match, ml);
				else memcpy(out, out - match, ml);
			}
		}
		out += ml;
	}

	return out - out_start;
}
