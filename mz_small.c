#include <stdint.h>
#include <string.h> /* memcpy() */
#include "mz.h"

#define read32(p) (*(uint32_t *) (p))

/* hash function used for the hashmap to find 4-byte string matches */
inline static uint16_t mz_hash4(uint32_t x) {
	return ((x << 19) - x) >> 16; /* knuth hash with mersenne prime */
}

/* fill hashmap with data from dictionary */
void mz_loaddict(mz_map_t *map, uint8_t *in, uint32_t in_len) {
	for (uint32_t i = 0; i + 4 <= in_len; ++i)
		(*map)[mz_hash4(read32(in + i))] = i;
}

/* output a sequence */
inline static uint8_t *mz_sequence(uint8_t *out, uint8_t *ls, uint32_t ll,
                                   uint32_t match, uint32_t ml) {
	uint8_t *token = out++;

	/* encode literal length */
	uint32_t llc = ll;
	if (llc < 15) { /* 0-14 are encoded in the token */
		*token = llc << 4;
	} else {
		*token = 15 << 4;
		if (llc < 255 + 15) { /* 15-269 are encoded with one extra byte */
			*out++ = llc - 15;
		} else { /* 270 and up are encoded with two extra bytes */
			llc -= 270;
			*out++ = 255;
			*out++ = llc; /* hmm, we don't need all possible values (yet) */
			*out++ = llc >> 8;
		}
	}

	/* write literal data */
	memcpy(out, ls, ll);
	out += ll;

	/* quit if we have no match part (end of compressed block) */
	if (!ml)
		return out;

	/* encode match length */
	ml -= 4;
	if (ml < 15) { /* 4-18 are encoded in the token */
		*token |= ml;
	} else {
		*token |= 15;
		if (ml < 255 + 15) { /* 19-273 encode with one extra byte */
			*out++ = ml - 15;
		} else { /* 274 and up are encoded with two extra bytes */
			ml -= 270;
			*out++ = 255;
			*out++ = ml;
			*out++ = ml >> 8;
		}
	}

	/* encode match offset, with LZ4 this comes before the extra length */
	--match; /* range is 1-0x10000 */
	*out++ = match;
	*out++ = match >> 8;

	return out;
}

/* count how long a match is, first 4 bytes are assumed to match */
inline static uint32_t mz_count(uint8_t *in, uint8_t *match, uint32_t ml_max) {
	uint32_t ml = 4;
	while (ml < ml_max && *(in + ml) == *(match + ml))
		++ml;
	return ml;
}

/* compress a block of data */
uint32_t mz_compress(mz_map_t *map, uint8_t *out, uint8_t *in, uint32_t in_len,
                     uint8_t *prev, uint32_t prev_len) {
	uint8_t *out_start = out, *in_start = in, *in_end = in + in_len;

	while (1) {
		uint32_t match, ml; /* match is uint32_t to avoid rollover */
		uint8_t *ls = in; /* if there's a literal, this is where it starts */

		/* go through input until we find a match */
		while (in + 4 <= in_end) {
			uint16_t hash = mz_hash4(read32(in));
			match = (*map)[hash];
			(*map)[hash] = in - in_start;
			if (match < in - in_start &&
			    read32(in) == read32(in_start + match)) {
				ml = mz_count(in, in_start + match, in_end - in);
				match = in - (in_start + match);
				break;
			}
			if (match + 4 <= prev_len &&
			    in - in_start + (prev_len - match) <= 0x10000 &&
			    read32(in) == read32(prev + match)) {
				uint32_t max = prev_len - match;
				if (max > in_end - in)
					max = in_end - in;
				ml = mz_count(in, prev + match, max);
				match = in - in_start + (prev_len - match);
				break;
			}
			//++in;
			in += 1 + ((in - ls) >> 4); /* skip incompressible data */
		}
		if (in + 4 > in_end) {
			if (ls < in_end)
				out = mz_sequence(out, ls, in_end - ls, 0, 0);
			return out - out_start;
		}
		/* hash matched part, slows down compressible data severely */
		//for (uint8_t *hs = in + 1; hs < in + ml && hs < in_end - 3; ++hs)
		//	(*map)[mz_hash4(read32(hs))] = hs - in_start;
		out = mz_sequence(out, ls, in - ls, match, ml);
		in += ml;
	}
}

/* copy data from left of the input pointer, overlapping makes patterns */
inline static void mz_offcpy(uint8_t *out, uint32_t off, uint32_t len) {
	uint8_t *out_end = out + len, *in = out - off;
	while (out < out_end)
		*out++ = *in++;
}

/* decompress a block of data */
uint32_t mz_decompress(uint8_t *out, uint32_t out_len,
                       uint8_t *prev, uint32_t prev_len,
                       uint8_t *in, uint32_t in_len) {
	uint8_t *out_start = out, *out_end = out + out_len, *in_end = in + in_len;

	while (in < in_end) {
		/* read a token and extra literal length bytes */
		uint8_t token = *in++; /* the loop ensures a byte is available */
		uint32_t ll = token >> 4;
		if (ll == 15 && in_end - in >= 3) {
			/* range check done combined, 3 bytes must exist for data anyway */
			ll += *in++;
			if (ll == 270) {
				ll += *in++;
				ll += *in++ << 8;
			}
		}

		/* output the literal */
		if (ll > in_end - in || ll > out_end - out)
			break;
		memcpy(out, in, ll);
		out += ll;
		in += ll;

		/* read match length */
		uint32_t ml = (token & 0x0F) + 4;
		if (ml == 19 && in_end - in >= 1) {
			ml += *in++;
			if (ml == 274 && in_end - in >= 2) {
				ml += *in++;
				ml += *in++ << 8;
			}
		}

		/* read the match offset */
		uint32_t match;
		if (in_end - in < 2)
			break;
		match = *in++;
		match |= *in++ << 8;
		++match;

		/* copy the match to the output (from earlier in the output) */
		if (match > out - out_start) {
			match -= out - out_start;
			if (match > prev_len || prev_len - match + ml > prev_len || 
			    ml > out_end - out)
				break;
			memcpy(out, prev + prev_len - match, ml);
		} else {
			if (match > out - out_start || ml > out_end - out)
				break;
			if (match < ml) /* we can't use memcpy for overlapping data */
				mz_offcpy(out, match, ml);
			else memcpy(out, out - match, ml);
		}
		out += ml;
	}

	return out - out_start;
}
