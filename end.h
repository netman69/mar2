#ifndef __END_H__
#define __END_H__

#include <stdint.h>

static inline void htobe(void *dst, uint64_t src, int l) {
	char *_dst = dst;
	while (l--)
		*_dst++ = src >> (8 * l);
}

static inline void htole(void *dst, uint64_t src, int l) {
	while (l--)
		((uint8_t *) dst)[l] = src >> (8 * l);
}

static inline uint64_t betoh(void *src, int l) {
	uint64_t ret = 0;
	char *_src = src;
	while (l--)
		ret |= (uint64_t) *_src++ << (8 * l);
	return ret;
}

static inline uint64_t letoh(void *src, int l) {
	uint64_t ret = 0;
	while (l--)
		ret |= (uint64_t) ((uint8_t *) src)[l] << (8 * l);
	return ret;
}

#endif /* __END_H__ */
