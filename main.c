/* mz - The MatZip file compressor.
 * 
 * Very similar to LZ4, but smaller and slower.
 * 
 * TODO 
 *   - change compressing message to "Compressing done." or sth at end
 *   - maybe we should be using crc32c instead of crc64, for speed
 *       or xxhash, it is faster... but i want my own stuff :P
 *   - read2 won't quit at EOF immediately, that ain't great
 */

#include <stdio.h> /* fprintf() */
#include <unistd.h> /* read(), write(), close(), getopt(), isatty(),
                       STDIN_FILENO, STDOUT_FILENO*/
#include <getopt.h> /* getopt() */
#include <fcntl.h> /* open() */
#include <sys/stat.h> /* fstat() */
#include <string.h> /* strerror() */
#include <errno.h> /* errno */
#include <stddef.h> /* NULL */
#include "mz.h"
#include "crc64.h"
#include "end.h"
#include "time64.h"

#define TPROGRESS (50) /* how many ms to wait between progress updates */

typedef struct __attribute__((__packed__)) {
	uint32_t magic, flags;
	uint64_t crc; /* always checked to ensure no false no-crc flag */
} header_t;

typedef struct __attribute__((__packed__)) {
	uint16_t len; /* 0 means end */
	uint8_t data[0xFFFF + 8]; /* + 8 for CRC */
} zdata_t;

#define MAGIC      (0x4d41545aU)
#define FLAG_NOCRC ((uint64_t) 1 << 31)

static uint8_t buf_uc[2][MZ_INPUTMAX(0xFFFF)];
static zdata_t zd;
static mz_map_t map = { 0 };

static int opt_crc = 1;
static int opt_force = 0;
static int opt_tty = 0;
static int opt_progress = 0;

static int fd_in = STDIN_FILENO, fd_out = STDOUT_FILENO;
static size_t in_total = 0, out_total = 0; /* only used for progress */

static inline ssize_t read2(int fd, void *out, size_t len) {
	size_t lr = 0;
	while (lr < len) {
		ssize_t l = read(fd, (char *) out + lr, len - lr);
		if (l == 0)
			return lr;
		if (l < 0)
			return l;
		lr += l;
	}
	return lr;
}

static size_t getsize(int fd) {
	struct stat sb;
	int ret = fstat(fd, &sb);
	if (ret < 0)
		return 0;
	return sb.st_size;
}

static char *progress_str = NULL; /* NULL indicates not initialized */
static uint64_t progress_tstart;
static size_t progress_fsize;
static int progress_spout;

static void progress(int force) {
	if (!opt_progress)
		return;
	static uint64_t tprev = 0;
	uint64_t t = time64();
	uint64_t dt = t - tprev;
	if (!force && dt < TPROGRESS * 1000)
		return;
	tprev = t;

	double speed = (t - progress_tstart) ?
	               ((double) (progress_spout ? out_total : in_total) /
	                (t - progress_tstart)) : 0;
	if (progress_fsize) {
		/* "\033[nA" goes n lines up, "\033[K" clears from cursor pos right */
		fprintf(stderr, "\033[3A\r%s [ %3llu%%, %5d ms, %8.3f MB/sec ]\033[K\n"
		                " %10zu / %zu bytes read\033[K\n"
		                " %10zu bytes written (%llu%% of input)\033[K\n",
		                progress_str, in_total * 100ULL / progress_fsize,
		                (int) (t - progress_tstart) / 1000,
		                speed, in_total, progress_fsize, out_total,
		                in_total ? (out_total * 100ULL / in_total) : 100);
	} else {
		fprintf(stderr, "\033[3A\r%s [ %5d ms, %8.3f MB/sec ]\033[K\n"
		                " %10zu bytes read\033[K\n"
		                " %10zu bytes written (%llu%% of input)\033[K\n",
		                progress_str, (int) (t - progress_tstart) / 1000,
		                speed, in_total, out_total,
		                in_total ? (out_total * 100ULL / in_total) : 100);
	}
}

static void progress_init(char *str, int spout) {
	if (!opt_progress)
		return;
	progress_str = str;
	progress_tstart = time64();
	progress_spout = spout;
	progress_fsize = getsize(fd_in);
	fprintf(stderr, "\n\n\n");
	progress(1);
}

static void error_report(char *msg, int use_errno) {
	if (opt_progress && progress_str != NULL)
		fprintf(stderr, "\033[3A\033[K");
	if (use_errno)
		fprintf(stderr, "error: %s: %s\n", msg, strerror(errno));
	else fprintf(stderr, "error: %s\n", msg);
	if (opt_progress && progress_str != NULL) {
		fprintf(stderr, "\n\n\n");
		progress_str = "Error!";
		progress(1);
	}
}

static int compress() {
	ssize_t len_out, len_in, len_in_prev = 0;
	uint64_t crc, crc_all = CRC64_INIT;
	int bn = 0;
	header_t hdr;
	if (!opt_tty && isatty(fd_out)) {
		error_report("refusing to write to tty, use -t to force", 0);
		return 1;
	}
	htobe(&hdr.magic, MAGIC, sizeof(hdr.magic));
	htole(&hdr.flags, opt_crc ? 0 : FLAG_NOCRC, sizeof(hdr.flags));
	htole(&hdr.crc, crc64(CRC64_INIT, &hdr, sizeof(hdr) - sizeof(hdr.crc)),
	      sizeof(hdr.crc));
	if ((len_out = write(fd_out, &hdr, sizeof(hdr))) < 0) {
		error_report("write", 1);
		return 1;
	}
	out_total += len_out;
	progress_init("Compressing...", 0);
	while (1) {
		progress(0);
		if ((len_in = read2(fd_in, buf_uc[bn], sizeof(buf_uc[0]))) < 0) {
			error_report("read", 1);
			return 1;
		}
		in_total += len_in;
		if (len_in == 0)
			break;
		len_out = mz_compress(&map, zd.data, buf_uc[bn],
		                      len_in, buf_uc[(bn + 1) % 2], len_in_prev);
		htole(&zd.len, len_out, sizeof(zd.len));
		if (opt_crc) {
			crc = crc64(CRC64_INIT, zd.data, len_out);
			crc_all = crc64_combine(crc_all, crc, len_out);
			htole(zd.data + len_out, crc, sizeof(crc));
		}
		if ((len_out = write(fd_out, &zd, len_out +
		                     (opt_crc ? sizeof(crc) : 0) +
		                     sizeof(zd.len))) < 0) {
			error_report("write", 1);
			return 1;
		}
		out_total += len_out;
		bn = (bn + 1) % 2;
		len_in_prev = len_in;
	}
	zd.len = 0;
	htole(&zd.data, crc_all, sizeof(crc_all));
	if ((len_out = write(fd_out, &zd, sizeof(zd.len) +
	                     (opt_crc ? sizeof(crc_all) : 0))) < 0) {
		error_report("write", 1);
		return 1;
	}
	out_total += len_out;
	progress(1);
	return 0;
}

static int decompress() {
	ssize_t len_in, len_out, len_out_prev = 0;
	uint64_t crc, crc_all = CRC64_INIT;
	int bn = 0, havecrc = 1;
	header_t hdr;
	if ((len_in = read2(fd_in, (void *) &hdr, sizeof(hdr))) < 0) {
		error_report("read", 1);
		return 1;
	}
	in_total += len_in;
	progress_init("Decompressing...", 1);
	if (len_in < sizeof(hdr)) {
		error_report("input truncated", 0);
		if (!opt_force)
			return 1;
	}
	if (betoh(&hdr.magic, sizeof(hdr.magic)) != MAGIC) {
		error_report("magic number incorrect", 0);
		if (!opt_force)
			return 1;
	}
	if (letoh(&hdr.crc, sizeof(hdr.crc)) !=
	    crc64(CRC64_INIT, &hdr, sizeof(hdr) - sizeof(hdr.crc))) {
		error_report("header checksum failed", 0);
		if (!opt_force)
			return 1;
	}
	if (letoh(&hdr.flags, sizeof(hdr.flags)) & FLAG_NOCRC)
		havecrc = 0;
	while (1) {
		progress(0);
		if ((len_in = read2(fd_in, &zd.len, sizeof(zd.len))) < 0) {
			error_report("read", 1);
			return 1;
		}
		in_total += len_in;
		if (len_in != 2) {
			error_report("input truncated", 0);
			if (!opt_force)
				return 1;
			if (len_in == 0) /* for the sake of predictability */
				((char *) &zd.len)[0] = 0;
			((char *) &zd.len)[1] = 0;
		}
		zd.len = letoh(&zd.len, sizeof(zd.len));
		if (zd.len == 0)
			break;
		size_t rlen = zd.len + (havecrc ? sizeof(crc) : 0);
		if ((len_in = read2(fd_in, zd.data, rlen)) < 0) {
			error_report("read", 1);
			return 1;
		}
		in_total += len_in;
		if (len_in != rlen) {
			error_report("input truncated", 0);
			if (!opt_force)
				return 1;
		}
		if (havecrc) {
			len_in -= sizeof(crc);
			if (opt_crc) {
				crc = crc64(CRC64_INIT, zd.data, len_in);
				crc_all = crc64_combine(crc_all, crc, len_in);
				if (crc != letoh(zd.data + len_in, sizeof(crc))) {
					error_report("checksum mismatch", 0);
					if (!opt_force)
						return 1;
				}
			}
		}
		len_out = mz_decompress(buf_uc[bn], sizeof(buf_uc[0]),
		                        buf_uc[(bn + 1) % 2], len_out_prev,
		                        zd.data, len_in);
		if (write(fd_out, buf_uc[bn], len_out) < 0) {
			error_report("write", 1);
			return 1;
		}
		out_total += len_out;
		bn = (bn + 1) % 2;
		len_out_prev = len_out;
	}
	if (havecrc) {
		if ((len_in = read2(fd_in, zd.data, sizeof(crc_all))) < 0) {
			error_report("read", 1);
			return 1;
		}
		in_total += len_in;
		if (len_in != sizeof(crc_all)) {
			error_report("input truncated", 0);
			if (!opt_force)
				return 1;
		}
		if (opt_crc && crc_all != letoh(zd.data, sizeof(crc_all))) {
			error_report("combined checksum mismatch", 0);
			if (!opt_force)
				return 1;
		}
	}
	if (read2(fd_in, zd.data, 1) != 0) {
		error_report("extra rubbish at end of input", 0);
		if (!opt_force)
			return 1;
	}
	progress(1);
	return 0;
}

static void usage(char *progname) {
	fprintf(stderr, "\n"
	        " ###################################\n"
	        " # mz - The MatZip file compressor #\n"
	        " ###################################\n\n"
	        "  usage: %s [-dnftph] [-o outfile] [infile] [outfile]\n\n"
	        "  -d            decompress\n"
	        "  -f            ignore errors when extracting\n"
	        "  -t            allow writing compressed data to tty\n"
	        "  -p            indicate progress\n"
	        "  -n            disable CRC creation / verification\n"
	        "  -h            show this message\n"
	        "  -o outfile    set output file yet allow stdin input\n"
	        "\n Copyright (C) 2021 Netman\n\n", progname);
}

#define out_opts (O_WRONLY | O_CREAT | O_TRUNC)
#define out_mode (0644)

int main(int argc, char *argv[]) {
	int direction = 0, opt, ret = 1;

	while ((opt = getopt(argc, argv, "dftpnho:")) != -1) {
		switch (opt) {
			case 'd': /* decompress */
				direction = 1;
				break;
			case 'f': /* ignore errors, useful for fuzzing or data recovery */
				opt_force = 1;
				break;
			case 't': /* allow writing compressed stuff to tty */
				opt_tty = 1;
				break;
			case 'p': /* indicate progress */
				opt_progress = 1;
				break;
			case 'n': /* do not calculate CRCs */
				opt_crc = 0;
				break;
			case 'h': /* show help */
				usage(argv[0]);
				ret = 0;
				goto quit;
			case 'o': /* specify output file, so you can combine with stdin */
				if (fd_out != STDOUT_FILENO) { /* prevent multiple -o */
					usage(argv[0]);
					goto quit;
				}
				if ((fd_out = open(optarg, out_opts, out_mode)) < 0) {
					error_report("open", 1);
					fd_out = STDOUT_FILENO;
					goto quit;
				}
				break;
			default:
				usage(argv[0]);
				goto quit;
		}
	}

	if (optind < argc && (fd_in = open(argv[optind], O_RDONLY)) < 0) {
		error_report("open", 1);
		fd_in = STDIN_FILENO;
		goto quit;
	}
	++optind;

	if (optind < argc && fd_out == STDOUT_FILENO &&
	    (fd_out = open(argv[optind], out_opts, out_mode)) < 0) {
		error_report("open", 1);
		fd_out = STDOUT_FILENO;
		goto quit;
	}
	++optind;

	if (optind < argc) {
		usage(argv[0]);
		goto quit;
	}

	if (direction == 0)
		ret = compress();
	else ret = decompress();

quit:
	if (fd_in != STDIN_FILENO)
		close(fd_in);
	if (fd_out != STDOUT_FILENO)
		close(fd_out);
	return ret;
}
