segment .text
global  mz_compress

;int mz_compress
mz_compress: ; TODO restore rbx, r12-r15, rsp, rbp
	; (short *map, char *out, char *in, int len, char *prev, int prev_len)
	; arguments in rdi, rsi, rdx, rcx, r8,   r9
	;              map, out, in,  len, prev, plen
	;uint8_t *out_start = out, *in_start = in, *in_end = in + in_len;
	add rcx, rdx ; rcx is input end
	mov r10, rdx ; r10 is input start

	;uint32_t ll, ml; /* we have to store up to and including 0x10000 */
	;uint32_t match; /* match is uint32_t to avoid rollover */
.next:
	;uint8_t *ls = in; /* if there's a literal, this is where it starts */
	mov r11, rdx ; r11 is start of literal
.loop:
	;/* go through input until we find a match */
	;while (in + 4 <= in_end) {
	mov rax, rdx
	add rax, 4
	cmp rcx, rax
	jc short .end
	;uint16_t hash = mz_hash4(read32(in) ^ *(in + 4));
	mov eax, [rdx]
	mov r15d, eax
	mov r13d, eax ; copy input for later
	shl eax, 19
	sub eax, r15d
	shr eax, 16
	movzx rax, ax
	;match = (*map)[hash];
	shl eax, 1
	add rax, rdi
	movzx r15, word [rax]
	;(*map)[hash] = in - in_start;
	mov r14, rdx
	sub r14, r10
	mov [rax], r14w
	;if (match < in - in_start &&
		;read32(in) == read32(in_start + match)) {
	add r15, r10
	cmp rdx, r15
	jle short .continue
	mov eax, [r15]
	cmp eax, r13d
	jne short .continue
	;ml = mz_count(in, in_start + match, in_end - in);
	; TODO
	;match = in - (in_start + match);
	mov rax, rdx
	sub rax, r15
	jmp short .match
	;}
.continue:
	;//++in;
	;in += 1 + ((in - ls) >> 4); /* skip incompressible data */
	inc rdx
	mov rax, rdx
	sub rax, r10
	shr rax, 4
	add rdx, rax
	;}
	jmp short .loop
.match:

	; TODO this is just for test
	mov rax, rdx
	sub rax, r10
	ret
		;ll = in - ls;
		;/* hash matched part, slows down compressible data severely */
		;//for (uint8_t *hs = in + 1; hs < in + ml && hs < in_end - 3; ++hs)
		;//	(*map)[mz_hash4(read32(hs))] = hs - in_start;
		;in += ml;
		;out = mz_sequence(out, ls, ll, match, ml);
	jmp short .next
.end:
	;if (ls < in_end)
	;out = mz_sequence(out, ls, in_end - ls, 0, 0);
	;return out - out_start;
	; mov rax, [outlength]
	xor rax, rax
	ret
